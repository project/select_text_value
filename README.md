# Select Text Value

This module provides additional widgets for the following field types:

- Text (formatted)
- Text (formatted, long)
- Text (plain)
- Text (plain, long)

The new widgets allow the site administrator to set some pre-defined values
for the text field that the user can then select from, using either a select
field or radio buttons.

Recommended that the "Allowed values" list does not use the key|label pattern
that is common with Drupal field settings. Instead, just provide one value per
line that will be both stored in the database and displayed on output.

The data is then saved to the database as the field would normally save it.

Similar to the "select_or_other" module, but with a fundamentally different
approach.

## Table of contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Installation

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-drupal-modules for
further information.

## Configuration

Choose the Select Text Value Widget in the content type form display settings.

## Maintainers

- Andy Giles - [andyg5000](https://www.drupal.org/u/andyg5000)
- Jonathan Daggerhart - [daggerhart](https://www.drupal.org/u/daggerhart)
